export default {
  siteName: 'IVF管理系统',
  // 医生模块
  doctor: {
    menu: '医生',
    firstName: '姓名'
  },
  // 预约模块
  appointment: {
    menu: '预约'
  },
}
