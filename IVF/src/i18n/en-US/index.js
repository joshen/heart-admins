export default {
  siteName: 'IVF Admin',
  login:{
    submitName: 'Login',
    password: 'Password',
  } ,
  // 医生模块
  doctor: {
    menu: 'My Doctor',
    firstName: 'First Name'
  },
  // 预约模块
  appointment: {
    menu: 'My Appointment'
  },
}
