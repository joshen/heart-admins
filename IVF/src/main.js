// ie polyfill
import '@babel/polyfill'

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/'
import { VueAxios } from './utils/request'
import axios from 'axios'
// import VueAxios from 'vue-axios'
import qs from 'qs'
// Vue.use(VueAxios, axios)
// import echarts from 'echarts'
// import htmlToPdf from './utils/htmlToPdf/htmlToPdf'

// mock
// import './mock'
import echarts from 'echarts'
Vue.prototype.$echarts = echarts;

import bootstrap from './core/bootstrap'
import './core/use'
import './permission' // permission control
import './utils/filter'

import './utils/install'
import VueI18n from 'vue-i18n'
import LangENUS from './i18n/en-US/index'
import LangZHCN from './i18n/zh-CN/index'
Vue.prototype.$qs = qs
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded' // global filter

Vue.config.productionTip = false

// Vue.prototype.$echarts = echarts

// mount axios Vue.$http and this.$http
Vue.use(VueAxios)
Vue.use(VueI18n)
const i18n = new VueI18n({
  locale: 'en-us',
  messages: {
    'en-us': LangENUS,
    'zh-cn': LangZHCN
  }
})
// Vue.use(htmlToPdf)

new Vue({
  router,
  store,
  i18n,
  created () {
    bootstrap()
  },
  render: h => h(App)
}).$mount('#app')
