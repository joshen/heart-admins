export function timeFix () {
  const time = new Date()
  const hour = time.getHours()
  return hour < 9 ? '早上好' : hour <= 11 ? '上午好' : hour <= 13 ? '中午好' : hour < 20 ? '下午好' : '晚上好'
}

export function welcome () {
  const arr = ['休息一会儿吧', '准备吃什么呢?', '要不要打一把 DOTA', '我猜你可能累了']
  const index = Math.floor(Math.random() * arr.length)
  return arr[index]
}

/**
 * 触发 window.resize
 */
export function triggerWindowResizeEvent () {
  const event = document.createEvent('HTMLEvents')
  event.initEvent('resize', true, true)
  event.eventType = 'message'
  window.dispatchEvent(event)
}

export function handleScrollHeader (callback) {
  let timer = 0

  let beforeScrollTop = window.pageYOffset
  callback = callback || function () {}
  window.addEventListener(
    'scroll',
    event => {
      clearTimeout(timer)
      timer = setTimeout(() => {
        let direction = 'up'
        const afterScrollTop = window.pageYOffset
        const delta = afterScrollTop - beforeScrollTop
        if (delta === 0) {
          return false
        }
        direction = delta > 0 ? 'down' : 'up'
        callback(direction)
        beforeScrollTop = afterScrollTop
      }, 50)
    },
    false
  )
}

/**
 * Remove loading animate
 * @param id parent element id or class
 * @param timeout
 */
export function removeLoadingAnimate (id = '', timeout = 1500) {
  if (id === '') {
    return
  }
  setTimeout(() => {
    document.body.removeChild(document.getElementById(id))
  }, timeout)
}

/**
 * 文件流下载，传入文件流和文件名称
 */
export function downLoadXls (data, filename) {
  // var blob = new Blob([data], {type: 'application/vnd.ms-excel'})接收的是blob，若接收的是文件流，需要转化一下
  if (typeof window.chrome !== 'undefined') {
    // Chrome version
    var link = document.createElement('a')
    link.href = window.URL.createObjectURL(data)
    link.download = filename
    link.click()
  } else if (typeof window.navigator.msSaveBlob !== 'undefined') {
    // IE version
    var blob = new Blob([data], { type: 'application/force-download' })
    window.navigator.msSaveBlob(blob, filename)
  } else {
    // Firefox version
    var file = new File([data], filename, { type: 'application/force-download' })
    window.open(URL.createObjectURL(file))
  }
}

/**
 *  当天、本周、本月、本年 开始和结束日期
 */
export function dateStartEndType (type) {
  var start = null; var end = null
  var date = new Date()
  var year = date.getFullYear()
  var month = date.getMonth() + 1
  var day = date.getDate()
  var week = date.getDay() // 今天本周的第几天

  var nowTime = date.getTime()
  var oneDayTime = 24 * 60 * 60 * 1000

  switch (type) {
    case 1:
      var str = year + '-' + month + '-' + day
      start = str // + ' 00:00:00'
      end = str // + ' 23:59:59'
      break
    case 2:
      var MondayTime = nowTime - (week - 1) * oneDayTime
      var monday = new Date(MondayTime)
      start = monday.getFullYear() + '-' + (monday.getMonth() + 1) + '-' + monday.getDate()
      end = year + '-' + month + '-' + day
      break
    case 3:
      start = year + '-' + month + '-' + '01'
      end = year + '-' + month + '-' + day
      break
    case 4:
      start = year + '-' + '01' + '-' + '01'
      end = year + '-' + month + '-' + day
      break
  }
  return {
    start: start,
    end: end
  }
}
