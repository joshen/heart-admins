import Vue from 'vue'
import axios from 'axios'
import store from '@/store'
import VueAxios from 'vue-axios'
// import {
//   VueAxios
// } from './axios'
import notification from 'ant-design-vue/es/notification'
import message from 'ant-design-vue/es/message'
import {
  ACCESS_TOKEN
} from '@/store/mutation-types'
Vue.use(VueAxios, axios)
// 创建 axios 实例
const env = process.env.NODE_ENV
var API_HOST = ''
if (env === 'development') {
  API_HOST = '/api'
} else {
  API_HOST = '/api'
}
const service = axios.create({
  baseURL: API_HOST, // api base_url
  timeout: 30000 // 请求超时时间
})

const err = (error) => {
  if (error.response) {
    const data = error.response.data
    const token = Vue.ls.get(ACCESS_TOKEN)
    if (error.response.status === 403) {
      notification.error({
        message: 'Forbidden',
        description: data.message
      })
    }
    if (error.response.status === 401 && !(data.result && data.result.isLogin)) {
      notification.error({
        message: 'Unauthorized',
        description: 'Authorization verification failed'
      })
      if (token) {
        store.dispatch('Logout').then(() => {
          setTimeout(() => {
            window.location.reload()
          }, 1500)
        })
      }
    }
  }
  return Promise.reject(error)
}

// request interceptor
service.interceptors.request.use(config => {
  const token = Vue.ls.get(ACCESS_TOKEN)
  if (token) {
    // config.headers['content-type'] = 'application/json' // 让每个请求携带自定义 token 请根据实际情况自行修改
    config.headers['X-Token'] = token // 让每个请求携带自定义 token 请根据实际情况自行修改
  }
  config.headers['Content-Type'] = 'application/json;charset=UTF-8'
  return config
}, err)

// response interceptor
service.interceptors.response.use((response) => {
  var _message = response.data.message
  if (response.data.code !== 0) {
    if (_message.indexOf('登录已超时') >= 0 || _message.indexOf('token 过期') >= 0 || _message.indexOf('token 无效') >= 0) {
      message.error('token已失效,请重新登陆')
      store.dispatch('Logout').then(() => {
        setTimeout(() => {
          window.location.reload()
        }, 500)
      })
    }
  }
  return response.data
}, err)

const installer = {
  vm: {},
  install (Vue) {
    Vue.use(VueAxios, service)
  }
}

export {
  installer as VueAxios,
  service as axios
}
