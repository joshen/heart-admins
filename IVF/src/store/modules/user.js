import Vue from 'vue'
import { login, ophonelogin, getInfo, logout } from '@/api/login'
import { ACCESS_TOKEN, ACCESS_ROLES, ACCESS_NAME, ACCESS_AVATAR, ACCESS_INFO } from '@/store/mutation-types'
import { welcome } from '@/utils/util'

const user = {
  state: {
    token: '',
    name: '',
    welcome: '',
    avatar: '',
    roles: [],
    info: {}
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_NAME: (state, { name, welcome }) => {
      state.name = name
      state.welcome = welcome
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
      Vue.ls.set(ACCESS_AVATAR, avatar, 7 * 24 * 60 * 60 * 1000)
    },
    SET_ROLES: (state, roles) => {
      state.roles = roles
    },
    SET_INFO: (state, info) => {
      state.info = info
    }
  },

  actions: {
    // 登录
    Login ({ commit }, userInfo) {
      return new Promise((resolve, reject) => {
        var loginType = login
        if (!userInfo.rand) loginType = ophonelogin
        loginType(userInfo).then(response => {
          if (response.code === 0) {
            Vue.ls.set(ACCESS_TOKEN, response.data.token, 7 * 24 * 60 * 60 * 1000)
            const userInfo = {
              id: response.data.id,
              phone: response.data.phone,
              name: response.data.name
            }
            Vue.ls.set(ACCESS_INFO, userInfo, 7 * 24 * 60 * 60 * 1000)
            Vue.ls.set(ACCESS_ROLES, response.data.permissions.split(','), 7 * 24 * 60 * 60 * 1000)
            commit('SET_TOKEN', response.data.token)
            commit('SET_ROLES', response.data.permissions.split(','))
            resolve(response)
          } else {
            resolve(response)
          }
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取用户信息
    GetInfo ({ commit }) {
      return new Promise((resolve, reject) => {
        getInfo().then(response => {
          const result = response.result
          if (result.role && result.role.permissions.length > 0) {
            const role = result.role
            role.permissions = result.role.permissions
            role.permissions.map(per => {
              if (per.actionEntitySet != null && per.actionEntitySet.length > 0) {
                const action = per.actionEntitySet.map(action => { return action.action })
                per.actionList = action
              }
            })
            role.permissionList = role.permissions.map(permission => { return permission.permissionId })
            commit('SET_ROLES', result.role)
            commit('SET_INFO', result)
          } else {
            reject(new Error('getInfo: roles must be a non-null array !'))
          }

          commit('SET_NAME', { name: result.name, welcome: welcome() })
          commit('SET_AVATAR', result.avatar)

          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 登出
    Logout ({ commit, state }) {
      return new Promise((resolve, reject) => {
        // commit('SET_ROLES', [])
        // commit('SET_AVATAR', '')

        // Vue.ls.remove(ACCESS_ROLES)
        // Vue.ls.remove(ACCESS_AVATAR)
        logout().then((res) => {
          commit('SET_TOKEN', '')
          Vue.ls.remove(ACCESS_TOKEN)
          Vue.ls.remove(ACCESS_NAME)
          Vue.ls.remove(ACCESS_INFO)
          resolve(res)
        }).catch((err) => {
          reject(err)
        })
      })
    }

  }
}

export default user
