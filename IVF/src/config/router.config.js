// eslint-disable-next-line
import { UserLayout, BasicLayout, RouteView, BlankLayout, PageView } from '@/layouts'

export const asyncRouterMap = [
  {
    path: '/',
    name: 'index',
    component: BasicLayout,
    meta: { title: '首页' },
    redirect: '/myDoctor',
    children: [
      // My Doctor
      {
        path: '/myDoctor',
        name: 'myDoctor',
        component: () => import('@/views/myDoctor/index'),
        meta: { title: 'My Doctor', keepAlive: true, icon: 'fork' }
      },
      // My Appointment
      {
        path: '/appointment',
        name: 'appointment',
        component: () => import('@/views/appointment/index'),
        hasPermission: 'home',
        meta: { title: 'My Appointment', keepAlive: true, icon: 'phone', permission: [ 'home' ] }
      },
      // User Management
      {
        path: '/userManagement',
        name: 'userManagement',
        component: () => import('@/views/userManagement/index'),
        // hasPermission: 'userManagement',
        meta: { title: 'User Management', keepAlive: true, icon: 'user', permission: [ 'home' ] }
      },
      // appointmentManagement
      {
        path: '/appointmentManagement',
        name: 'appointmentManagement',
        component: () => import('@/views/appointmentManagement/index'),
        // hasPermission: 'userManagement',
        meta: { title: 'Appointment Management', keepAlive: true, icon: 'calendar', permission: [ 'home' ] }
      },
      // Clinic profile Management
      {
        path: '/clinicprofile',
        name: 'clinicprofile',
        component: () => import('@/views/clinicprofile/index'),
        // hasPermission: 'userManagement',
        meta: { title: 'Clinic profile Management', keepAlive: true, icon: 'home', permission: [ 'home' ] }
      },
      // admin Management
      {
        path: '/adminmanagement',
        name: 'adminmanagement',
        component: () => import('@/views/adminmanagement/index'),
        // hasPermission: 'userManagement',
        meta: { title: 'Admin Management', keepAlive: true, icon: 'team', permission: [ 'appManage' ] }
      }
    ]
  },
  {
    path: '*', redirect: '/404', hidden: true
  }
]

/**
 * 基础路由
 * @type { *[] }
 */
export const constantRouterMap = [
  {
    path: '/',
    component: UserLayout,
    redirect: '/login',
    hidden: true,
    children: [
      {
        path: '/login',
        name: 'login',
        component: () => import(/* webpackChunkName: "user" */ '@/views/login/Login')
      }
    ]
  },
  {
    path: '/',
    component: UserLayout,
    redirect: '/register',
    hidden: true,
    children: [
      {
        path: '/register',
        name: 'register',
        component: () => import(/* webpackChunkName: "user" */ '@/views/login/register')
      }
    ]
  },
  {
    path: '/',
    component: UserLayout,
    redirect: '/modifyPassword',
    hidden: true,
    children: [
      {
        path: '/modifyPassword',
        name: 'modifyPassword',
        component: () => import(/* webpackChunkName: "user" */ '@/views/login/modifyPassword')
      }
    ]
  }
]
