import api from './index'
import { axios } from '@/utils/request'
// import qs from 'qs'

/**
 * @param parameter
 * @returns {*}
 */

// 登录
// {
//   "email": "admin@example.com",
//   "password": "PASSWORD"
// }
export function login (parameter) {
  return axios({
    url: api.UserLogin,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 邮箱查重
// /auth/check/email?email=chris@gmail.com
export function repeatEmail (parameter) {
  return axios({
    url: api.emailrepeat,
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 忘记密码发起
// ```json
// {
//     "email": "xxx"
// }
export function forgetpsw (parameter) {
  return axios({
    url: api.forgetpsw,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
}

// 忘记密码验证
// /auth/forgetpass/check?token=XXX
export function forgetpswcheck (parameter) {
  return axios({
    url: api.forgetpswcheck,
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 忘记密码重置
// {
//   "token": "XXX"
//   "new_password": "XXX"
// }
export function forgetpswreset (parameter) {
  return axios({
    url: api.forgetpswreset,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
}


export function userInfo () {
  var accessName = localStorage.getItem('pro__Access-Name')
  if (accessName) {
    return JSON.parse(accessName).value
  } else {
    return ''
  }
}

export function getSmsCaptcha (parameter) {
  return axios({
    url: api.SendSms,
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

export function getInfo () {
  return axios({
    url: '/user/info',
    method: 'get',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
}

export function logout () {
  return axios({
    url: api.Logout,
    method: 'post',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
}

/**
 * get user 2step code open?
 * @param parameter {*}
 */
export function get2step (parameter) {
  return axios({
    url: api.twoStepCode,
    method: 'post',
    data: parameter
  })
}
/**
 * @param parameter
 * @returns {*}
 */

export function ophonelogin (parameter) {
  return axios({
    url: api.ophoneLogin,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 修改密码
export function editpwd (parameter) {
  return axios({
    url: api.editpwd,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

export function info () {
  return axios({
    url: '/admin/info',
    method: 'get',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
}
