const api = {
  UserLogin: '/auth/signin', // 登陆
  emailrepeat: '/auth/check/email', // email是否重复
  // 用户注册
  forgetpswinit: '/auth/forgetpass/init',// 忘记密码
  forgetpswcheck: '/auth/forgetpass/check', // 忘记密码验证
  forgetpswreset: '/auth/forgetpass/reset', // 忘记密码重置






  Login: '/auth/login',
  ForgePassword: '/auth/forge-password',
  Register: '/auth/register',
  twoStepCode: '/auth/2step-code',

  SendSmsErr: '/account/sms_err',
  // get my info

  ophoneLogin: '/admin/ophonelogin', // 短信登陆

  SendSms: '/admin/msgcodeverify', // 手机验证码
  editpwd: '/admin/editpwd', // 修改密码

  info: '/admin/info', // 菜单获取
  Logout: '/admin/logout', // 登出
  detail: '/admin/detail', // 用户详情

  create: '/admin/create', // 系统管理-管理员管理-添加
  updata: '/admin/update', // 系统管理-管理员管理-修改
  adminlist: '/admin/list', // 系统管理-管理员管理-列表查询
  adminDetail: '/admin/detail', // 系统管理-管理员管理-获取详情
  adminDel: '/admin/delete', // 系统管理-管理员管理-删除

  addrole: '/role/create', // 系统管理-角色管理-添加
  updatarole: '/role/update', // 系统管理-角色管理-修改
  rolelist: '/role/list', // 系统管理-角色管理-列表查询
  roleDetail: '/role/detail', // 系统管理-管理员管理-获取详情
  roleDel: '/role/delete', // 系统管理-管理员管理-删除

  addApp: '/apply/create', // app管理-添加
  appList: '/apply/list', // app管理-列表
  appUpdata: '/apply/update', // app管理-修改
  appdetail: '/apply/detail', // app管理-app详情
  appEnabled: '/apply/enabled', // app管理-启用/禁用
  appDel: '/apply/delete', // app管理-列表删除单条

  setrole: '/admin/setrole', // 系统管理-管理员管理--分配角色保存
  adminsroleidlist: '/admin/adminsroleidlist', // 系统管理-管理员管理--已选角色保存

  addUser: '/user/create', // 用户管理-添加用户
  updateUser: '/user/update', // 用户管理-修改用户
  listUser: '/user/list', // 用户管理-用户列表
  userdetail: '/user/detail', // 用户管理-修改

  addPhone: '/user/code/create', // 用户管理-登录方式-新增手机号
  getAllphone: '/user/code/list', // 用户管理-登录方式-获取所有手机号
  updatePhone: '/user/code/update', // 用户管理-登录方式-编辑或修改密码
  delPhone: '/user/delete', // 用户管理-列表删除
  enabled: 'user/enabled', // 用户管理 -启用禁用
  delCodePhone: '/user/code/delete', // 用户管理-登录方式-编辑或修改密码

  versionlist: 'policy/version/list', // 网关-版本-列表
  versionupdata: '/policy/version/update', // 网关-版本-修改
  versioncreate: '/policy/version/create', // 网管-版本-添加
  versiondetil: '/policy/version/detail', // 网管-版本-添加
  copyList: '/policy/version/copyList', // 网管-版本-版本号列表

  strategylist: '/policy/list', // 网关 - 版本-策略列表
  strategyCreate: '/policy/create', // 网关 - 版本- 策略创建
  strategydetail: 'policy/detail', // 网关 - 版本- 策略详情
  strategyUpdata: 'policy/update', // 网关 - 版本- 策略修改
  strategyDel: 'policy/delete', // 网关 - 版本- 策略删除

  gatewayList: '/policy/gateway/list', // 网关发布--网关列表
  gatewayDetail: '/policy/gateway/detail', // 网关发布--网关详情
  gatewayVersion: '/policy/gateway/gatewayVersion', // 网关发布--网关使用版本信息
  gatewayInfo: '/policy/gateway/publishInfo', // 网关发布--当前发布版本信息
  gatewayCreate: '/policy/gateway/create', // 网关发布--新增网关
  gatewayDel: '/policy/gateway/delete', // 网关发布--删除网关
  gatewayUpdate: '/policy/gateway/update', // 网关发布--修改网关
  gatewayState: '/policy/gatewayState/update', // 网关发布--禁用/启用网关
  releaseVersion: '/policy/version/publish', // 发布版本

  appParamList: '/apply/appParamList' // 传参

}
export default api
