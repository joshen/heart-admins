import api from './index'
import { axios } from '@/utils/request'
// 系统管理-管理员管理-添加
export function Create (parameter) {
  return axios({
    url: api.create,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}
// 系统管理-管理员管理-修改
export function Updata (parameter) {
  return axios({
    url: api.updata,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 系统管理-管理员管理-列表查询
export function adminList (parameter) {
  return axios({
    url: api.adminlist,
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 系统管理-管理员管理-列表详情
export function adminDetail (parameter) {
  return axios({
    url: api.adminDetail,
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 系统管理-管理员管理-列表删除
export function adminDel (parameter) {
  return axios({
    url: api.adminDel,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 系统管理-角色管理-添加
export function addRole (parameter) {
  return axios({
    url: api.addrole,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}
// 系统管理-角色管理-修改
export function updataRole (parameter) {
  return axios({
    url: api.updatarole,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}
// 系统管理-角色管理-查询
export function roleList (parameter) {
  return axios({
    url: api.rolelist,
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
}

// 系统管理-角色管理-角色详情
export function roleDetail (parameter) {
  return axios({
    url: api.roleDetail,
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 系统管理-角色管理-列表删除
export function roleDel (parameter) {
  return axios({
    url: api.roleDel,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// app管理-添加
export function addApp (parameter) {
  return axios({
    url: api.addApp,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// app管理-修改
export function appdetail (parameter) {
  return axios({
    url: api.appdetail,
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// app管理-删除
export function appDel (parameter) {
  return axios({
    url: api.appDel,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}
// app管理-修改
export function appEnabled (parameter) {
  return axios({
    url: api.appEnabled,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}
// app管理-修改
export function appUpdata (parameter) {
  return axios({
    url: api.appUpdata,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}
// app管理-查询
export function appList (parameter) {
  return axios({
    url: api.appList,
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    }
  })
}

// 系统管理-管理员管理-分配角色保存
export function setrole (parameter, data) {
  return axios({
    url: api.setrole,
    method: 'post',
    params: parameter,
    data: data,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 系统管理-管理员管理-分配角色保存
export function adminsroleidlist (parameter) {
  return axios({
    url: api.adminsroleidlist,
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 用户管理-用户列表
export function listUser (parameter) {
  return axios({
    url: api.listUser,
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 用户管理-修改用户
export function updateUser (parameter) {
  return axios({
    url: api.updateUser,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}
// 用户管理-添加用户
export function addUser (parameter, data) {
  return axios({
    url: api.addUser,
    method: 'post',
    params: parameter,
    data: data,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}
export function userdetail (parameter) {
  return axios({
    url: api.userdetail,
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 用户管理-新增手机号
export function addPhone (parameter) {
  return axios({
    url: api.addPhone,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 用户管理-登录方式-获取所有手机号
export function getAllphone (parameter) {
  return axios({
    url: api.getAllphone,
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 用户管理-新增手机号
export function updatePhone (parameter) {
  return axios({
    url: api.updatePhone,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 用户管理-新增手机号
export function delPhone (parameter) {
  return axios({
    url: api.delPhone,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 用户管理-添加用户
export function enabled (parameter, data) {
  return axios({
    url: api.enabled,
    method: 'post',
    params: parameter,
    data: data,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 用户管理-新增手机号
export function delCodePhone (parameter) {
  return axios({
    url: api.delCodePhone,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 网关 版本 列表
export function versionlist (parameter) {
  return axios({
    url: api.versionlist,
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 网关 版本 修改
export function versionupdata (parameter) {
  return axios({
    url: api.versionupdata,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 网关 版本 新建
export function versioncreate (parameter) {
  return axios({
    url: api.versioncreate,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}
// 网关 版本 详情
export function versiondetil (parameter) {
  return axios({
    url: api.versiondetil,
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 网关 版本 版本号列表
export function copyList () {
  return axios({
    url: api.copyList,
    method: 'get',
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 网关 版本 策略列表
export function strategylist (parameter) {
  return axios({
    url: api.strategylist,
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 网关 版本 策略创建
export function strategyCreate (parameter) {
  return axios({
    url: api.strategyCreate,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 网关 版本 策略详情
export function strategydetail (parameter) {
  return axios({
    url: api.strategydetail,
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 网关 版本 策略创建
export function strategyUpdata (parameter) {
  return axios({
    url: api.strategyUpdata,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 网关 版本 策略删除
export function strategyDel (parameter) {
  return axios({
    url: api.strategyDel,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 网关发布--网关创建
export function gatewayCreate (parameter) {
  return axios({
    url: api.gatewayCreate,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 网关发布 -网关列表
export function gatewayList () {
  return axios({
    url: api.gatewayList,
    method: 'get',
    // params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 网关发布 -网关更新
export function gatewayUpdate (parameter) {
  return axios({
    url: api.gatewayUpdate,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 网关 网关发布- 网关列表单体详情
export function gatewayDetail (parameter) {
  return axios({
    url: api.gatewayDetail,
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 网关发布 -网关更新
export function gatewayDel (parameter) {
  return axios({
    url: api.gatewayDel,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 网关发布 -网关禁用启用
export function gatewayState (parameter) {
  return axios({
    url: api.gatewayState,
    method: 'post',
    data: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 网关 网关发布- 当前版本
export function gatewayInfo (parameter) {
  return axios({
    url: api.gatewayInfo,
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}
// 网关 网关发布- 当前版本
export function releaseVersion (parameter) {
  return axios({
    url: api.releaseVersion,
    method: 'post',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}
// 网关 网关发布- 当前版本
export function gatewayVersion (parameter) {
  return axios({
    url: api.gatewayVersion,
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}

// 网关 网关发布- 当前版本
export function appParamList (parameter) {
  return axios({
    url: api.appParamList,
    method: 'get',
    params: parameter,
    headers: {
      'Content-Type': 'application/json;charset=UTF-8'
    }
  })
}
