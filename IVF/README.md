概述
-----
“应用系统总集套件” 是专为集成/管理腾云公司自研、合作研发、外包研发、直接采购等方式获得的各类不同web应用系统而设计开发的一套系统集成解决方案。该套件是具备各类应用系统的统一集成、统一运维、统一用户管理等功能完整的集成管理解决方案，能有效的集成整合各类差异化的应用系统，具有集成简单，运维管理便捷等特性。该套件集统一网关接入、统一用户管理/登录鉴权、标准门户系统易用的应用系统集成、运营管理套件。

Overview
----

基于 [Ant Design of Vue](https://vuecomponent.github.io/ant-design-vue/docs/vue/introduce-cn/) 实现的 



环境和依赖
----

- node
- yarn
- webpack
- eslint
- @vue/cli ~3
- [ant-design-vue](https://github.com/vueComponent/ant-design-vue) - Ant Design Of Vue 实现
- [vue-cropper](https://github.com/xyxiao001/vue-cropper) - 头像裁剪组件
- [@antv/g2](https://antv.alipay.com/zh-cn/index.html) - Alipay AntV 数据可视化图表
- [Viser-vue](https://viserjs.github.io/docs.html#/viser/guide/installation)  - antv/g2 封装实现

> 请注意，我们强烈建议本项目使用 [Yarn](https://yarnpkg.com/) 包管理工具，这样可以与本项目演示站所加载完全相同的依赖版本 (yarn.lock) 。由于我们没有对依赖进行强制的版本控制，采用非 yarn 包管理进行引入时，可能由于 Pro 所依赖的库已经升级版本，而引入了新版本所照成的问题。作者可能会由于时间问题无法及时排查而导致您采用本项目作为基项目而出现问题。



项目下载和运行
----

- 拉取项目代码
```bash
git clone https://git.ybsjyyn.com/yyn/integration-suit-login-web.git
cd integration-suit-login-web
```

- 安装依赖
```
yarn install / npm install
```

- 开发模式运行
```
yarn run serve
```

- 编译项目
```
yarn run build
```

- Lints and fixes files
```
yarn run lint
```



- **使用polyfill兼容至 IE10**

- 移除polyfill。 polyfill用于兼容IE，不需要兼容IE可移除。减少体积
  > 参考 [Vue CLI Polyfill](https://cli.vuejs.org/zh/guide/browser-compatibility.html#usebuiltins-usage)
  
  - 移除入口文件的 `import '@babel/polyfill'`
    
  - 删除 `babel.conflg.js` 中的
    ```ecmascript 6
    [
      '@babel/preset-env',
      {
        'useBuiltIns': 'entry'
      }
    ]
